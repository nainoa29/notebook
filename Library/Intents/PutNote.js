const GetNote = require('./GetNote')
const Note = require('../Models/Note')

class PutNote {

    TABLE_NAME = 'notes'

    /**
     *
     * @param event
     */
    constructor(event) {
        this.event = event;
    }

    /**
     *
     * @returns {Promise<*>}
     */
    async execute() {
        try {
            let getNote = new GetNote(this.event)

            let note = await getNote.execute()

            /**
             * Build the params
             * @type {{TableName: string, Key: {id: {N}}}}
             */
            let params = this.buildParams(note, this.event.body)

            /**
             * Execute call against the db client
             */
            let result = await this.callClient(params, this.event.db)

            return new Note(this.event.marshaller.unmarshall(result.Attributes))
        }catch(error) {
            console.log(error)
            throw error
        }
    }

    /**
     *
     * @param params
     * @param dynamoDbClient
     * @returns {Promise<*>}
     */
    async callClient(params, dynamoDbClient) {
        try {
            return await dynamoDbClient.updateItem(params).promise();
        }catch(error) {
            return error
        }
    }

    /**
     *
     * @param note
     * @param event
     * @returns {{Item: DynamoDB.AttributeMap, TableName: string, ReturnConsumedCapacity: string}}
     */
    buildParams(note, newNote) {
        let __that = this;
        return {
            Key: __that.event.marshaller.marshall({
                id: note.id
            }),
            ExpressionAttributeNames: {
              "#M": "message",
              "#DT": "datetime"
            },
            ExpressionAttributeValues: __that.event.marshaller.marshall({
              ":m": newNote.message,
              ":dt": newNote.datetime
            }),
            TableName: __that.TABLE_NAME,
            UpdateExpression: "SET #M = :m, #DT = :dt",
            ReturnValues: 'ALL_NEW'
        };
    }
}

module.exports = PutNote;