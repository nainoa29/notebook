var Note = require('../Models/Note')

class ListNotes {

    TABLE_NAME = 'notes'

    /**
     *
     * @param event
     * @param dynamoDbClient
     * @param marshaller
     */
    constructor(event, dynamoDbClient, marshaller) {
        this.event = event;
        this.dynamoDbClient = dynamoDbClient
        this.marshaller = marshaller
    }

    /**
     *
     * @returns {Promise<Note|*>}
     */
    async execute() {
        try {
            const params = this.buildParams(this.event)

            let record = await this.callClient(params, this.dynamoDbClient)

            return new Note(this.marshaller.unmarshall(record.Item));
        }catch(error) {
            throw error
        }
    }

    /**
     *
     * @param params
     * @param dynamoDbClient
     * @returns {Promise<*|*>}
     */
    async callClient(params, dynamoDbClient) {
        try {
            return await dynamoDbClient.getItem(params).promise();
        }catch(error) {
            return error
        }
    }

    /**
     *
     * @param event
     * @returns {{TableName: string, Key: {id: {N}}}}
     */
    buildParams(event) {
        let __that = this
        return {
            Key: {
                "id": {
                    N: event.pathParameters.id
                }
            },
            TableName: __that.TABLE_NAME
        }
    }
}

module.exports = GetNote;