var Note = require('../Models/Note')

class GetNotes {

    TABLE_NAME = 'notes'

    /**
     *
     * @param event
     */
    constructor(event) {
        this.event = event;
    }

    /**
     *
     * @returns {Promise<Note|*>}
     */
    async execute() {
        try {
            const params = this.buildParams(this.event)

            let records = await this.callClient(params, this.event.db)

            if(!records.Items) {
                return []
            }

            return this.formatCollection(records.Items, this.event.marshaller)
        }catch(error) {
            throw error
        }
    }

    /**
     * Unmarshall the collection
     *
     * @param collection
     * @param marshaller
     * @returns {*}
     */
    formatCollection(collection, marshaller) {
        collection.forEach(function(part, index) {
            this[index] = new Note(marshaller.unmarshall(part))
        }, collection)

        return collection
    }

    /**
     *
     * @param params
     * @param dynamoDbClient
     * @returns {Promise<*|*>}
     */
    async callClient(params, dynamoDbClient) {
        try {
            return await dynamoDbClient.scan(params).promise();
        }catch(error) {
            return error
        }
    }

    /**
     *
     * @param event
     * @returns {{TableName: string, Key: {id: {N}}}}
     */
    buildParams(event) {
        let __that = this
        return {
            TableName: __that.TABLE_NAME
        }
    }
}

module.exports = GetNotes;