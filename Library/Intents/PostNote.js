const crypto = require('crypto');

class PostNote {

    TABLE_NAME = 'notes'

    /**
     *
     * @param event
     */
    constructor(event) {
        this.event = event;
    }

    /**
     *
     * @returns {Promise<*>}
     */
    async execute() {
        try {
            /**
             * Build the params
             * @type {{TableName: string, Key: {id: {N}}}}
             */
            let params = this.buildParams(this.event.body)

            /**
             * Execute call against the db client
             */
            return await this.callClient(params, this.event.db)
        }catch(error) {
            throw error
        }
    }

    /**
     *
     * @param params
     * @param dbClient
     * @returns {Promise<*>}
     */
    async callClient(params, dbClient) {
        try {
            return await dbClient.putItem(params).promise();
        }catch(error) {
            throw error
        }
    }

    /**
     *
     * @param post
     * @returns {{TableName: string, Key: {id: {N}}}}
     */
    buildParams(post) {
        let __that = this;
        return {
            Item: __that.event.marshaller.marshall({
                "id": crypto.randomBytes(16).toString("hex"),
                "message": post.message,
                "datetime": post.datetime
            }),
            ReturnConsumedCapacity: "TOTAL",
            TableName: __that.TABLE_NAME
        };
    }
}

module.exports = PostNote;