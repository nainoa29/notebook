var Note = require('../Models/Note')
var _ = require('lodash')
var RecordNotFoundError = require('../../Errors/RecordNotFoundError')

class GetNote {

    TABLE_NAME = 'notes'

    /**
     *
     * @param event
     */
    constructor(event) {
        this.event = event;
    }

    /**
     *
     * @returns {Promise<Note|*>}
     */
    async execute() {
        try {
            const params = this.buildParams(this.event)

            let record = await this.callClient(params, this.event.db)

            if(_.isEmpty(record)) {
                throw new RecordNotFoundError('Failed to locate note. ID: ' + this.event.pathParameters.id)
            }

            return new Note(this.event.marshaller.unmarshall(record.Item));
        }catch(error) {
            throw error
        }
    }

    /**
     *
     * @param params
     * @param dynamoDbClient
     * @returns {Promise<*|*>}
     */
    async callClient(params, dynamoDbClient) {
        try {
            return await dynamoDbClient.getItem(params).promise();
        }catch(error) {
            throw error
        }
    }

    /**
     *
     * @param event
     * @returns {{TableName: string, Key: {id: {N}}}}
     */
    buildParams(event) {
        let __that = this
        return {
            Key: event.marshaller.marshall({
                id: event.pathParameters.id.toString()
            }),
            TableName: __that.TABLE_NAME
        }
    }
}

module.exports = GetNote;