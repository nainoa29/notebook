const GetNote = require('./GetNote')
const Note = require('../Models/Note')
const RecordNotFoundError = require('../../Errors/RecordNotFoundError')

class DeleteNote {

    TABLE_NAME = 'notes'

    /**
     *
     * @param event
     */
    constructor(event) {
        this.event = event;
    }

    /**
     *
     * @returns {Promise<*|undefined>}
     */
    async execute() {
        try {
            let getNote = new GetNote(this.event)
            let note = await getNote.execute()

            /**
             * Validate that the resource exists
             */
            if(!(note instanceof Note)) {
                throw new RecordNotFoundError('This resource does not exist. ID: ' + this.event.pathParameters.id)
            }

            /**
             * Build params for delete operation
             *
             * @type {{TableName: string, Key: DynamoDB.AttributeMap}}
             */
            let params = this.buildParams(this.event)

            /**
             * Call the delete operation on the client
             */
            let result = await this.callClient(params, this.event.db)

            return null;
        } catch(error) {
            throw error
        }
    }

    /**
     *
     * @param params
     * @param dbClient
     */
    async callClient(params, dbClient) {
        try {
            return await dbClient.deleteItem(params).promise();
        } catch(error) {
            throw error
        }
    }

    /**
     *
     * @param event
     * @returns {{TableName: string, Key: DynamoDB.AttributeMap}}
     */
    buildParams(event) {
        let __that = this
        return {
            Key: event.marshaller.marshall( {
                id: event.pathParameters.id
            }),
            TableName: __that.TABLE_NAME
        };
    }
}

module.exports = DeleteNote;