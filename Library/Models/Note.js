class Note {
    constructor(props) {
        this.id = props.id
        this.message = props.message
        this.datetime = props.datetime
    }

    id() {
        return this.id
    }

    message() {
        return this.message
    }

    datetime() {
        return this.datetime
    }
}

module.exports = Note;