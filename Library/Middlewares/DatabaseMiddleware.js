const AWS = require('aws-sdk')
const _ = require('lodash')

module.exports = () => {
    const db = new AWS.DynamoDB({
        apiVersion: '2012-08-10',
        region: 'us-east-1'
    })

    /**
     *
     * @param request
     * @returns {Promise<*>}
     */
    const beforeRequest = async (request) => {
        _.assign(request.event, {db: db})
        _.assign(request.event, {marshaller: AWS.DynamoDB.Converter})
    }

    /**
     * Remove database object from request
     *
     * @param request
     * @returns {Promise<void>}
     */
    const afterRequest = async (request) => {
        _.omit(request.event, 'db')
        _.omit(request.event, 'marshaller')
    }

    return {
        before: beforeRequest,
        after: afterRequest
    }
}