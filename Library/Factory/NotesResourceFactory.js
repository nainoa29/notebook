const PostNote = require('../Intents/PostNote');
const GetNotes = require('../Intents/GetNotes');

class NoteHandler {

    post(event) {
        return new PostNote(event)
    }

    get(event) {
        return new GetNotes(event)
    }

    make(event) {
        switch(event.requestContext.http.method) {
            case 'POST':
                return this.post(event)
            case 'GET':
                return this.get(event)
        }
    }

}

module.exports = NoteHandler;