const GetNote = require('../Intents/GetNote');
const PutNote = require('../Intents/PutNote');
const DeleteNote = require('../Intents/DeleteNote');

class NoteHandler {

    put(event) {
        return new PutNote(event)
    }

    get(event) {
        return new GetNote(event)
    }

    delete(event) {
        return new DeleteNote(event)
    }

    make(event) {
        switch(event.requestContext.http.method) {
            case 'PUT':
                return this.put(event)
            case 'GET':
                return this.get(event)
            case 'DELETE':
                return this.delete(event)
        }
    }

}

module.exports = NoteHandler;