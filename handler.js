'use strict';

const NoteActionFactory = require('./Library/Factory/NoteActionFactory')
const NoteResourceFactory = require('./Library/Factory/NotesResourceFactory')
const RecordNotFoundError = require('./Errors/RecordNotFoundError')

const middy = require('@middy/core')
const jsonBodyParser = require('@middy/http-json-body-parser')
const httpErrorHandler = require('@middy/http-error-handler')
const databaseMiddleware = require('./Library/Middlewares/DatabaseMiddleware')

/**
 * Handles Note resource actions
 *
 * @param event
 * @returns {Promise<{body: string, statusCode: number}>}
 */
const note = async (event) => {
  try {
    let factory = new NoteActionFactory()

    let intent = factory.make(event)

    const data = await intent.execute()

    return { statusCode: 200, body: JSON.stringify(data) }
  } catch (error) {
    let statusCode = 400

    if(error instanceof RecordNotFoundError) {
      statusCode = 404
    }

    return { statusCode: statusCode, body: JSON.stringify({
      message: error.message
    })}
  }
};

const noteHandler = middy(note)
    .use(jsonBodyParser())
    .use(databaseMiddleware())
    .use(httpErrorHandler())

/**
 * Handles Note resource actions
 *
 * @param event
 * @returns {Promise<{body: string, statusCode: number}>}
 */
const notes = async (event) => {
  try {
    let factory = new NoteResourceFactory()
    let intent = factory.make(event)

    const data = await intent.execute()

    return { statusCode: 200, body: JSON.stringify(data) }
  } catch (error) {
    let statusCode = 400

    if(error instanceof RecordNotFoundError) {
      statusCode = 404
    }

    return { statusCode: statusCode, body: JSON.stringify({
        message: error.message
    })}
  }
};

const notesHandler = middy(notes)
    .use(jsonBodyParser())
    .use(databaseMiddleware())
    .use(httpErrorHandler())

module.exports = { noteHandler, notesHandler }


